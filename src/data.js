const data = {
  products : [
    {
      id: '1',
      name: 'Laptop',
      price: 14000000,
      image: 'https://akcdn.detik.net.id/visual/2020/06/02/09fb4d2c-e9d4-4949-b26d-969983c4bd54_169.jpeg?w=650'
    },
    {
      id: '2',
      name: 'Handphone',
      price: 10000000,
      image: 'https://t-2.tstatic.net/palembang/foto/bank/images/xiaomi-mi-max-3_20180726_121005.jpg'
    },
    {
      id: '3',
      name: 'Power Bank',
      price: 500000,
      image: 'https://www.koran.id/wp-content/uploads/2021/12/Merek-Power-Bank-Murah-scaled.jpg'
    },
  ],
}

export default data;
